/*
 * relay.cpp
 *
 * Arduino Program for auto-gardening project
 *
 * (c) Kevin Brunet
 * 25/08/18
 * released into the public domain. If you use this, please let me know
 * (just out of pure curiosity!) by sending me an email:
 * brunet.kevin6@gmail.com
 *
 */
#include "grow_conf.h"
DS3231  rtc3(SDA, SCL);

extern int the_click;

void  setup3() {
  rtc3.begin();
}

int  onHour(char *on) {
  String times = rtc3.getTimeStr();

  if (times[0] == on[0] && times[1] == on[1] && times[3] == on[2] && times[4] == on[3]) {
    return(1);
  }
  else {
    return(0);  
  }
}

int  offHour(char *off) {
  String times = rtc3.getTimeStr();

  if (times[0] == off[0] && times[1] == off[1] && times[3] == off[2] && times[4] == off[3]) {
    return(1);
  }
  else {
    return(0);  
  }
}

void  interuptor() {
  setup3();
  if (onHour(ON) == 1) {
    the_click = 255;
  }
  if (offHour(OFF) == 1) {
    the_click = 0;  
  }
  digitalWrite(RELAY, the_click);
}
