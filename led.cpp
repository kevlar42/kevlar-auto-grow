/*
 * led.cpp
 *
 * Arduino Program for auto-gardening project
 *
 * (c) Kevin Brunet
 * 25/08/18
 * released into the public domain. If you use this, please let me know
 * (just out of pure curiosity!) by sending me an email:
 * brunet.kevin6@gmail.com
 *
 */
#include "grow_conf.h"

int offset = 0;

void  blinkMe() {
  if (offset % 2 == 0) {
    digitalWrite(LED, HIGH); 
  }
  else {
    digitalWrite(LED, LOW);  
  }
  offset++;
  if (offset == 4) {
    offset = 0;  
  }
}
