/*
 * lcd.cpp
 *
 * Arduino Program for auto-gardening project
 *
 * (c) Kevin Brunet
 * 25/08/18
 * released into the public domain. If you use this, please let me know
 * (just out of pure curiosity!) by sending me an email:
 * brunet.kevin6@gmail.com
 *
 */
#include "grow_conf.h"

//DS3231  rtc2(SDA, SCL);
const int rs = RS, en = EN, d4 = D4, d5 = D5, d6 = D6, d7 = D7;
LiquidCrystal lcd2(rs, en, d4, d5, d6, d7);
DS3231  rtc2(SDA, SCL);

void  setup2() {
  lcd2.begin(16, 2);
  rtc2.begin();
}

void  firstLine(int val) {
  setup2();
  lcd2.clear();
  lcd2.setCursor(0, 0);
  if (val <= 40) {
    lcd2.clear();
    lcd2.print("->  ");
    lcd2.print(rtc2.getTimeStr()); 
    lcd2.print("  <-");
  }
  if (val >= 40) {
    lcd2.clear();
    lcd2.print(rtc2.getDOWStr());  
  }
  if (val >= 50) {
    lcd2.clear();
    lcd2.print("-> ");
    lcd2.print(rtc2.getDateStr());
    lcd2.print(" <-");
    
  }
}

void  secondLine(int val, char *on, char *off) {
  lcd2.setCursor(0, 1);
  if (val <= 30) {
    lcd2.print("ON Time : ");
    lcd2.print(on[0]);
    lcd2.print(on[1]);
    lcd2.print('h');
    lcd2.print(on[2]);
    lcd2.print(on[3]);

  }
  else {
    lcd2.print("OFF Time : ");
    lcd2.print(off[0]);
    lcd2.print(off[1]);
    lcd2.print('h');
    lcd2.print(off[2]);
    lcd2.print(off[3]);
  }
}

void  toLeft() {
  // scroll 40 positions (string length) to the left
  // to move it offscreen left:
  for (int positionCounter = 0; positionCounter < 40; positionCounter++) {
    // scroll one position left:
    lcd2.scrollDisplayLeft();
    // wait a bit:
    delay(40);
  }
  delay(2000);
}

void  toRight() {
  for (int positionCounter = 0; positionCounter < 29; positionCounter++) {
    // scroll one position right:
    lcd2.scrollDisplayRight();
    // wait a bit:
    delay(150);
  }
}