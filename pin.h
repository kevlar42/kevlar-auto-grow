/*
 * pin.h
 *
 * Arduino Program for auto-gardening project
 *
 * (c) Kevin Brunet
 * 25/08/18
 * released into the public domain. If you use this, please let me know
 * (just out of pure curiosity!) by sending me an email:
 * brunet.kevin6@gmail.com
 *
 */
#ifndef pin_h
#define pin_h
// define pins for LCD
#define RS 12
#define EN 11
#define D4 5
#define D5 4
#define D6 3
#define D7 2
// define pin for relay
#define RELAY 7
// define pin for LED
#define LED 8

#endif