/*
 * kevlar_garden.ino
 *
 * Arduino Program for auto-gardening project
 *
 * (c) Kevin Brunet
 * 25/08/18
 * released into the public domain. If you use this, please let me know
 * (just out of pure curiosity!) by sending me an email:
 * brunet.kevin6@gmail.com
 *
 */
#include "grow_conf.h"

// LCD interface pin
// with the arduino pin number it is connected to
const int rs = RS, en = EN, d4 = D4, d5 = D5, d6 = D6, d7 = D7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// Init the DS3231 using the hardware interface
DS3231  rtc(SDA, SCL);

// global value for the relay at start : 255 to turn on, 0 to turn off
int the_click = 0;
char  *on = ON;
char  *off = OFF;

void setup()
{
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.setCursor(3,0);
  lcd.print("Hello  :-)");
  lcd.setCursor(1,1);
  lcd.print("kevlar garden");
  toLeft();
  // Set pinout for relay
  pinMode(RELAY, OUTPUT);
  // Set pinout for led
  pinMode(LED, OUTPUT);
  // Setup Serial connection
  //Serial.begin(115200);
  
  // Initialize the rtc object
  rtc.begin();
  
  // The following lines can be uncommented to set the date and time
  //rtc.setDOW(WEDNESDAY);     // Set Day-of-Week to SUNDAY
  //rtc.setTime(00, 29, 00);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(29, 8, 2018);   // Set the date to January 1st, 2014
}

int i = 0;

void loop()
{ 
  // Send fist line
  firstLine(i); 
  // print second line
  secondLine(i, ON, OFF);
  // set led on/off
  //blinkMe();
  //chech hour for relay
  interuptor();
  // increment i and push to 0 every 60 sec
  i++;
  if (i == 60) {
    i = 0;
  }
  // Wait one second before repeating :)
  delay (1000);
}
