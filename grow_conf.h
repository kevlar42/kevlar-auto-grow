/*
 * grow_conf.h
 *
 * Arduino Program for auto-gardening project
 *
 * (c) Kevin Brunet
 * 25/08/18
 * released into the public domain. If you use this, please let me know
 * (just out of pure curiosity!) by sending me an email:
 * brunet.kevin6@gmail.com
 *
 */
#ifndef grow_conf_h
#define grow_conf_h

#include <LiquidCrystal.h>
#include <Wire.h>
#include "pin.h"
#include "DS3231.h"

#define ON "0600" //define on hour like 17h48m
#define	OFF "0000" //define on hour like 17h48m

void  firstLine(int val);
void  secondLine(int val, char *on, char *off);
void  toLeft();
void  toRight();

int  onHour(char *on);
int  offHour(char *off);
void  interuptor();

void  blinkMe();

#endif //__configuration_H
